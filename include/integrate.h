/* Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 */
/* 
 * integrate.hpp
 * 
 * This file is part of the lifetime distribution
 *     (https://gitlab.com/ladeynov.d/lifetime).
 * Copyright (c) 2021 Dmitry Ladeynov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cmath>
#include "../include/parameters.h"

void integrate(DataState * data, NormalParameters * p)
{
    long double phase_integral_fst  = 0;
    long double phase_integral_scd  = 0;

    long double rate_integral_fst = 0;
    long double rate_integral_scd = 0;

    long double phase_mv_i_sw = 0;
    long double rate_mv_i_sw  = 0;
    long double phase_sd_i_sw = 0;
    long double rate_sd_i_sw  = 0;

    for(size_t i = 0; i < p->sw_count; ++i) {
        phase_integral_fst += data[i].phase_mv;
        phase_integral_scd += data[i].phase_sd;

        rate_integral_fst += data[i].rate_mv;
        rate_integral_scd += data[i].rate_sd;

        phase_mv_i_sw += data[i].phase_i_sw;
        rate_mv_i_sw  += data[i].rate_i_sw;
    }

    phase_integral_fst /= p->sw_count;
    phase_integral_scd /= p->sw_count;
    rate_integral_fst /= p->sw_count;
    rate_integral_scd /= p->sw_count;
    phase_mv_i_sw /= p->sw_count;
    rate_mv_i_sw  /= p->sw_count;

    data[0].phase_pb = 1.0 - phase_integral_fst / p->t_size;
    data[0].rate_pb  = 1.0 - rate_integral_fst / p->t_size;

    data[0].phase_mv = phase_integral_fst * p->t_step;
    data[0].phase_sd = p->t_step * sqrt(
        2.0 * phase_integral_scd - phase_integral_fst * phase_integral_fst);

    data[0].rate_mv = rate_integral_fst * p->t_step;
    data[0].rate_sd = p->t_step * sqrt(
        2.0 * rate_integral_scd - rate_integral_fst * rate_integral_fst);

    for(size_t i = 0; i < p->sw_count; ++i) {
        phase_sd_i_sw += pow((data[i].phase_i_sw - phase_mv_i_sw), 2);
        rate_sd_i_sw  += pow((data[i].rate_i_sw  - rate_mv_i_sw), 2);
    }

    data[0].phase_mv_i_sw = phase_mv_i_sw;
    data[0].rate_mv_i_sw  = rate_mv_i_sw;
    data[0].phase_sd_i_sw = sqrt(phase_sd_i_sw / p->sw_count);
    data[0].rate_sd_i_sw  = sqrt(rate_sd_i_sw / p->sw_count);
}
