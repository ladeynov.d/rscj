/* Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 */
/* 
 * parameters.hpp
 * 
 * This file is part of the lifetime distribution
 *     (https://gitlab.com/ladeynov.d/lifetime).
 * Copyright (c) 2021 Dmitry Ladeynov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <cmath>
//#include <omp.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "data_structures.h"

#pragma once

#ifdef FP32
    typedef float fp_;
#else
    typedef double fp_;
#endif

namespace parameters
{
    //std::string out_file_name = "out.txt";

    double calc_asym_coef(double b)
    {
        double xcount = 100000;
        double step = 2.0 * M_PI / xcount;
        double x = -M_PI;

        std::vector<double> y;
        for(size_t i = 0; i < xcount; ++i, x += step)
            y.emplace_back(sin(x) + b * sin(2 * x));

        return std::abs(2.0 / (2.0 * (*std::max_element(y.begin(), y.end())) ));
    }

    void real2normal(RealParameters * rp, NormalParameters * np)
    {
        // Constants
        // CI = 1e-6  // [mkA]
        // CC = 1e-12 // [pF]
        // CT = 1e-3  // [mK]

        // e = 1.602176634e-19 | [C]   Electron charge
        // h = 1.054571817e-34 | [J*s] Plank constant (reduced)
        // k = 1.380649e-23    | [J/K] Botzman constant

        const double c_s2eh  = 55122907194.91327;     // sqrt(2 * e / h) * sqrt(CI / CC) 
        const double c_1s2eh = 18.141278297678028;    // c_s2eh / (2 * e * CI / h)
        const double c_2ekh  = 4.195150167862804e-05; // 2 * e * k / h

        // Plasma freq.
        rp->plasma_freqency = c_s2eh * sqrt(rp->i_crit / rp->capacity) / 
                (2.0 * M_PI);
        np->t_step = rp->t_step;
        np->i_bias = rp->i_bias / rp->i_crit;
        np->i_osc = rp->i_osc * rp->i_crit;
        np->omega = rp->freqency / rp->plasma_freqency;
        np->alpha = c_1s2eh / (rp->resistance * sqrt(rp->i_crit * rp->capacity));
        np->gamma = c_2ekh * rp->temp / rp->i_crit;
        np->i_vel = rp->i_vel / (rp->i_crit * rp->plasma_freqency);

        np->t_size    = static_cast<size_t>(rp->t_max / rp->t_step);
        np->intensity = sqrt(2.0 * np->alpha * np->gamma * rp->t_step);

        np->asym_coef = calc_asym_coef(rp->snd_harm);
        np->r_window = 1.0 / (30e-9 * rp->plasma_freqency / rp->t_step);
        np->sw_count = rp->sw_count;
        np->threads = rp->threads;
        
        //std::cout << "np->i_bias " << np->i_bias << '\n' <<
                     //"np->i_osc  " << np->i_osc  << '\n' <<
                     //"np->omega" <<   np->omega << '\n' <<
                     //"np->alpha" <<  np->alpha << '\n' <<
                     //"np->gamma" <<  np->gamma << '\n' <<
                     //"np->i_vel"  << np->i_vel << '\n' <<
                     //"np->t_size"   <<   np->t_size << '\n' <<
                     //"np->intensity" <<  np->intensity << '\n' <<
                     //"np->asym_coef" <<  np->asym_coef << '\n' <<
                     //"np->r_window" <<   np->r_window << '\n' <<
                     //"np->sw_count" <<    np->sw_count << '\n' <<
                     //"np->threads" <<   np->threads << '\n';

    }

    void normal2real(DataState * d, RealParameters * rp)
    {
        d->phase_mv      = d->phase_mv / rp->plasma_freqency;
        d->phase_sd      = d->phase_sd / rp->plasma_freqency;
        d->phase_pb      = d->phase_pb;
        d->phase_mv_i_sw = d->phase_mv_i_sw * rp->i_crit;
        d->phase_sd_i_sw = d->phase_sd_i_sw * rp->i_crit;
        d->rate_mv       = d->rate_mv / rp->plasma_freqency;
        d->rate_sd       = d->rate_sd / rp->plasma_freqency;
        d->rate_pb       = d->rate_pb;
        d->rate_mv_i_sw  = d->rate_mv_i_sw * rp->i_crit;
        d->rate_sd_i_sw  = d->rate_sd_i_sw * rp->i_crit;
    }

    void read(int argc, char * argv[], RealParameters * rp, 
            NormalParameters * np, CalcUnit * c)
    {
        rp->i_crit     = std::stod(argv[1]);
        rp->i_bias     = std::stod(argv[2]);
        rp->i_osc      = std::stod(argv[3]);
        rp->i_vel      = std::stod(argv[4]);
        rp->resistance = std::stod(argv[5]);
        rp->capacity   = std::stod(argv[6]);
        rp->freqency   = std::stod(argv[7]);
        rp->temp       = std::stod(argv[8]);
        rp->snd_harm   = std::stod(argv[9]);
        rp->t_max      = std::stod(argv[10]);
        rp->t_step     = std::stod(argv[11]);
        rp->sw_count   = std::stoi(argv[12]);
        rp->threads    = std::stoi(argv[13]);
        rp->scd_flag   = std::stoi(argv[14]);

        rp->file_name = argv[15];

        c->platform   = std::stoi(argv[16]);
        c->device     = std::stoi(argv[17]);

    }

    void print(DataState * d,  RealParameters * rp, NormalParameters * np)
    {
        std::stringstream header_stream;
        header_stream << std::fixed
            << "i_crit"          << '\t' << "i_bias"   << '\t'
            << "i_osc"           << '\t' << "i_vel"    << '\t'
            << "resistance"      << '\t' << "capacity" << '\t'
            << "temp"            << '\t' << "freqency" << '\t'
            << "plasma_freqency" << '\t' << "snd_harm" << '\t'
            << "alpha"    << '\t' << "gamma"  << '\t' << "omega"    << '\t'
            << "t_max"    << '\t' << "t_step" << '\t' << "sw_count" << '\t'
            << "phase_mv" << '\t'
            << "phase_sd" << '\t'
            << "phase_pb" << '\t'
            << "phase_mv_i_sw" <<'\t'
            << "phase_sd_i_sw" <<'\t'
            << "rate_mv" << '\t'
            << "rate_sd" << '\t'
            << "rate_pb" << '\t'
            << "rate_mv_i_sw" << '\t'
            << "rate_sd_i_sw" << '\n';

        std::stringstream data_stream;
        data_stream << std::fixed << std::scientific
            << rp->i_crit     << '\t' << rp->i_bias   << '\t'
            << rp->i_osc      << '\t' << rp->i_vel    << '\t'
            << rp->resistance << '\t' << rp->capacity << '\t'
            << rp->temp       << '\t' << rp->freqency << '\t'
            << rp->plasma_freqency << '\t' << rp->snd_harm << '\t'
            << np->alpha   << '\t' << np->gamma  << '\t' << np->omega    << '\t'
            << rp->t_max   << '\t' << rp->t_step << '\t' << rp->sw_count << '\t'
            << d->phase_mv << '\t'
            << d->phase_sd << '\t'
            << d->phase_pb << '\t'
            << d->phase_mv_i_sw << '\t'
            << d->phase_sd_i_sw << '\t'
            << d->rate_mv << '\t'
            << d->rate_sd << '\t'
            << d->rate_pb << '\t'
            << d->rate_mv_i_sw << '\t'
            << d->rate_sd_i_sw << '\n';

        //std::string file_name = out_file_name;

#ifdef OUT_CONSOLE
        std::cout << stream.str();
#endif
        std::ofstream fout0("header.txt");
        fout0 << header_stream.str();
        fout0.close();

        std::ofstream fout1(rp->file_name);
        fout1 << data_stream.str();
        fout1.close();
    }

    void print_i_sw(DataState * d, RealParameters * rp)
    {
        std::stringstream data_stream;
        std::string scd_file_name = std::string(rp->file_name) + std::string("_scd");

        std::ofstream fout(scd_file_name);

        data_stream << "phase_i_sw" << '\t' << "rate_i_sw" << '\n';

        for(size_t i = 0; i < rp->sw_count; ++i)
            data_stream << std::fixed << std::scientific
                    << d[i].phase_i_sw * rp->i_crit << '\t'
                    << d[i].rate_i_sw  * rp->i_crit << '\n';

        fout << data_stream.str();
        fout.close();
    }
}
