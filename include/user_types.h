// Licensed after GNU GPL v3

#pragma once

#ifdef FP32
    typedef float fp_;
#else
    typedef double fp_;
#endif
