// Licensed after GNU GPL v3

#pragma once

typedef struct {
    unsigned x[5];
    unsigned counter;
} XorwowState;

// The state array must be initialized to not be all zero in the 
// first four words
unsigned xorwow_gen(XorwowState * state)
{
    // Algorithm "xorwow" from p. 5 of Marsaglia, "Xorshift RNGs"
    unsigned t  = state->x[4];
    // Perform a contrived 32-bit shift
    unsigned s  = state->x[0];
    state->x[4] = state->x[3];
    state->x[3] = state->x[2];
    state->x[2] = state->x[1];
    state->x[1] = s;
 
    t ^= t >> 2;
    t ^= t << 1;
    t ^= s ^ (s << 4);
    state->x[0] = t;
    state->counter += 362437;

    // Convert rnd number to float
    return (t + state->counter);
}

void normal_noise_gen(XorwowState * xws, float * noise_buffer, size_t size)
{
    float r    = 0.0f;
    float phi  = 0.0f;
    unsigned a = 0; 
    unsigned b = 0;

    for(unsigned i = 0; i < size; i += 2) {
        // Gen. rnd num.
        a = xorwow_gen(xws);
        b = xorwow_gen(xws);

        // Convert to (0, 1] float
        noise_buffer[i]     = ((float)a + 1.0f) / 4294967296.0f;
        noise_buffer[i + 1] = ((float)b + 1.0f) / 4294967296.0f;

        //Box-Muller Trans.
        r   = -2.0f * log(noise_buffer[i]);
        r   = sqrt(r);
        phi = 2.0f * M_PI * noise_buffer[i + 1];

        noise_buffer[i]     = (r * cos(phi));
        noise_buffer[i + 1] = (r * sin(phi));
    }
}
