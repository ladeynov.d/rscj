// Licensed after GNU GPL v3

#pragma once

typedef struct {
    double phase_pb;
    double phase_mv;
    double phase_sd;
    double phase_i_sw;
    double phase_mv_i_sw;
    double phase_sd_i_sw;
    double rate_pb;
    double rate_mv;
    double rate_sd;
    double rate_i_sw;
    double rate_mv_i_sw;
    double rate_sd_i_sw;
    unsigned seed;
} DataState;

typedef struct {
    double i_crit;
    double i_bias;
    double i_osc;
    double i_vel;
    double resistance;
    double capacity;
    double freqency;
    double plasma_freqency;
    double temp;
    double snd_harm;
    double t_max;
    double t_step;
    size_t sw_count;
    int threads;
    int scd_flag;
    char * file_name;
} RealParameters;

typedef struct {
    double i_bias;
    double i_osc;
    double alpha;
    double i_vel;
    double gamma;
    double omega;
    double intensity;
    double snd_harm;
    double asym_coef;
    double r_window;
    double t_step;
    size_t t_size;
    size_t sw_count;
    int threads;
    int scd_flag;
} NormalParameters;

typedef struct {
    int platform;
    int device;
} CalcUnit;
