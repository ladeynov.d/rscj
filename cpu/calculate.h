/* Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 */
/* 
 * calculate.hpp
 * 
 * This file is part of the lifetime distribution
 *      (https://gitlab.com/ladeynov.d/lifetime).
 * Copyright (c) 2021 Dmitry Ladeynov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
 
#pragma once

#include <random>
#include <omp.h>

#include "../include/parameters.h"

#include "kernel.h"

void calculate(DataState * data, NormalParameters * p, CalcUnit * c)
{
    omp_set_num_threads(p->threads);

#pragma omp parallel num_threads(p->threads)
{
    for(size_t i = 0; i < p->sw_count; ++i)
        data[i].seed = i * time(0);

    #pragma omp for simd schedule(static, 10)
    for(size_t i = 0; i < p->sw_count; ++i)
        stormer_verlet(data[i], p);
}
}
