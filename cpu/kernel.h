// Licensed after GNU GPL v3

#pragma once

#include <cmath>

#include "../include/data_structures.h"
#include "../include/xorwow_gen.h"
#include "../include/user_types.h"

#define BUFF_SIZE 10

typedef struct {
    double time;
    double i_sw;
    bool   flag;
} Result;

void stormer_verlet(DataState& data_buffer, NormalParameters * p)
{
    fp_ c_s_h = p->t_step * 0.5;
    fp_ c_b   = 1.0 / (1.0 + p->alpha * c_s_h);
    fp_ c_a   = c_b / (1.0 + p->alpha * c_s_h);
    fp_ c_s_b = c_b * p->t_step;

    fp_ c_b_intensity = c_b * p->intensity;
    fp_ c_s_vel   = p->t_step * p->i_vel;
    fp_ c_s_omega = p->t_step * p->omega;

    fp_ fi      = asin(p->i_bias);
    fp_ v       = 0.0;
    fp_ fi_next = 0.0;

    // precalc coef. var
    fp_ sin_fi      = 0.0;
    fp_ sin_fi_next = 0.0;
    fp_ i_sw        = 0.0;

    double p_bound = M_PI + asin(p->i_osc - p->i_bias);
    Result phase = {.time = 0.0, .i_sw = 0.0, .flag = true};

    double v_bound = 0.5 / p->alpha;
    Result rate  = {.time = 0.0, .i_sw = 0.0, .flag = true};
    // Exponential smoothing, V(t) var.
    fp_ v_smth  = 0;
    fp_ r_window = p->r_window;

    XorwowState xws;
    xws.x[0]    = data_buffer.seed;
    xws.counter = 0;

    float noise_buffer[BUFF_SIZE];

    fp_ tmp_value = 0.0;
    size_t accum = 0;

#ifdef DRLZ
    std::ofstream file("phase_voltage.csv");
    file << "time" << '\t' << "phase" << '\t'<< "voltage" << '\t' 
            << "voltage_smth" << '\t' << "i_sw" << '\n';
#endif

    for(size_t i = 0; i < p->t_size; i += BUFF_SIZE) {

        if(!rate.flag || i_sw > 1.0)
            break;

        normal_noise_gen(&xws, noise_buffer, BUFF_SIZE);

        for(size_t j = 0; j < BUFF_SIZE; ++j) {

            i_sw = p->i_bias + c_s_vel * accum;

            tmp_value = c_b_intensity * noise_buffer[j] + c_s_b * i_sw;
            tmp_value += c_s_b * p->i_osc * sin(c_s_omega * accum);

            sin_fi = sin(fi);
            sin_fi = p->asym_coef * (sin_fi + p->snd_harm * sin(2.0 * fi));

            fi_next = fi + c_s_b * (v - c_s_h * sin_fi) + c_s_h * tmp_value;

            sin_fi_next = sin(fi_next);

            sin_fi = p->asym_coef * (sin_fi_next + 
                    p->snd_harm * sin(2.0 * fi_next));

            v = c_a * v - c_s_h * (c_a * sin_fi + sin_fi_next) + tmp_value;

            fi = fi_next;

            // Voltage exponential smoothing
            v_smth += r_window * (v - v_smth);
#ifndef DRLZ
            if(v_smth < v_bound && v_smth > -v_bound && rate.flag) {
                rate.time = accum;
                rate.i_sw = i_sw;
            } else {
                rate.flag = false;
            }

            p_bound = M_PI + asin(p->i_osc - i_sw);
            if(fi < p_bound && fi > -p_bound && phase.flag) {
                phase.time = accum;
                phase.i_sw = i_sw;
            } else {
                phase.flag = false;
            }
#else
            file << accum << '\t' << fi << '\t'<< v << '\t' << v_smth << '\t' << i_sw << '\n';
#endif
            accum++;
        }
    }
#ifdef DRLZ
    file.close();
#endif

#ifdef PHASE_CALC
    data_buffer.phase_mv   = phase.time;
    data_buffer.phase_sd   = phase.time * (phase.time + 1.0) / 2.0;
    data_buffer.phase_i_sw = phase.i_sw;
#endif
    data_buffer.rate_mv    = rate.time;
    data_buffer.rate_sd    = rate.time * (rate.time + 1.0) / 2.0;
    data_buffer.rate_i_sw  = rate.i_sw;
}
