# RSCJ-model
## Description

The RCSJ model of resistively and capacitively shunted Josephson junctions

## Dependence
- g++
- make
- OpenMP
- OpenCL

## Build
Build with  
```  
$sh build.sh
```
or run **make** with for each Makefile.[arc]_[precision]

arc = {cpu, gpu}

prec = {fp32, fp64}

## Run
List of parameters:
```
1.  i_crit        [mkA]
2.  i_bias        [mkA]
3.  i_osc         [double, part of i_crit]
4.  i_vel         [mkA/s]
5.  resistance    [Ohm]
6.  capacity      [pF]
7.  freqency      [Hz]
8.  temp          [mK]
9.  snd_harm      [double]
10. t_max         [double]
11. t_step        [double]
12. sw_count      [int]
13. threads       [int]
14. scd_flag      [int]
15. out_file_name [string]
16. platform_id   [int]
17. device_id     [int]
```

for example:

Lifetime:
```
./rcsj_gpu_fp32 10 3 0 0 30 2.65 0 100 0 100000 0.5 2000 32 1 file_0 0 0
```
SCD:
```
./rcsj_gpu_fp32 10 0 0 10 30 2.65 0 100 0 100000 0.5 2000 32 1 file_0 0 0
```
