/* Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 */
/* 
 * lifetime.cpp
 * 
 * This file is part of the lifetime distribution
 *      (https://gitlab.com/ladeynov.d/lifetime).
 * Copyright (c) 2021 Dmitry Ladeynov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <chrono>

#include "include/parameters.h"
#include "include/integrate.h"

#ifdef CLGPU
    #include "gpu/calculate.h"
#else
    #include "cpu/calculate.h"
#endif

int main(int argc, char * argv[]) {

    RealParameters   * rp = new RealParameters;
    NormalParameters * np = new NormalParameters;
    CalcUnit   * c = new CalcUnit;

    parameters::read(argc, argv, rp, np, c);
    parameters::real2normal(rp, np);

#ifdef TIME_CHECK
    auto begin_time = std::chrono::high_resolution_clock::now();
#endif

    DataState * data_buffer = new DataState[rp->sw_count];

    calculate(data_buffer, np, c);
    integrate(data_buffer, np);

    parameters::normal2real(&data_buffer[0], rp);
    parameters::print(&data_buffer[0], rp, np);

    if(rp->scd_flag)
        parameters::print_i_sw(data_buffer, rp);

    delete [] data_buffer;
    delete rp;
    delete np;
    delete c;

#ifdef TIME_CHECK
    auto end_time = std::chrono::high_resolution_clock::now();
    auto time = static_cast<double>(std::chrono::duration_cast<std::chrono::
        milliseconds>(end_time - begin_time).count()) / 1000.0;

    std::cerr << "TIME: " << std::fixed 
        << std::setprecision(6) << time << " s\n";
#endif
    return 0;
}

