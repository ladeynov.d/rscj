/* Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 */
/* 
 * calculate.hpp
 * 
 * This file is part of the lifetime distribution (https://gitlab.com/ladeynov.d/lifetime).
 * Copyright (c) 2021 Dmitry Ladeynov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include <string>
#include <random>
#include <cmath>

#define CL_HPP_HMINIMUM_OPENCL_VERSION 300
#define CL_HPP_TARGET_OPENCL_VERSION 300

#include <CL/opencl.hpp>
#include "../include/data_structures.h"

static std::string load_kernel(const std::string path)
{
    std::string kernel_string;
    std::ifstream file(path);

    if (!file) {
        std::cout << '\n'<< path << "kernel file dose't exist\n";
        kernel_string = "";
    }

    while(!file.eof()) {
        std::string tmp;
        std::getline(file, tmp);
        kernel_string += tmp + "\n";
    }

    file.close();
    return kernel_string;
}

void calculate(DataState * data_buffer, NormalParameters * p, CalcUnit * c)
{
// Prepare DataState seed parameters
    for(size_t i = 0; i < p->sw_count; ++i) {
        data_buffer[i].seed = i * time(0);
    }

// OpenCL section

    std::vector<cl::Platform> all_platforms;
    cl::Platform::get(&all_platforms);

    if (all_platforms.size() == 0) {
        std::cout<<" No platforms found. Check OpenCL installation!\n";
        exit(1);
    }

    cl::Platform default_platform=all_platforms[c->platform];

    std::vector<cl::Device> all_devices;
    default_platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);
    if(all_devices.size() == 0){
        std::cout <<" No devices found. Check OpenCL installation!\n";
        exit(1);
    }

    cl::Device default_device=all_devices[c->device];
    cl::Context context({default_device});

    // create buffers on device (allocate space on GPU)

    cl::Buffer p_mem_obj(context, CL_MEM_READ_ONLY, sizeof(NormalParameters));

    cl::Buffer data_mem_obj(context, CL_MEM_READ_WRITE, 
        p->sw_count * sizeof(DataState));

    cl::CommandQueue queue(context, default_device);

    // create the program that we want to execute on the device
    cl::Program::Sources sources;

    std::string kernel_file_name = "gpu/kernel.c";

    std::string kernel_code = "#define LOCAL_SIZE " + 
            std::to_string(p->threads) + '\n';
#ifdef FP32
    kernel_code.append("#define FP32\n");
#endif
    if(p->i_osc > 0.0)
        kernel_code.append("#define IOSC\n");
    if(p->snd_harm > 0.0)
        kernel_code.append("#define IASYM\n");

    kernel_code.append(load_kernel(kernel_file_name));
    sources.push_back({kernel_code.c_str(), kernel_code.length()});

    std::string build_cl_options= "";

    cl::Program program(context, sources);
    if (program.build({default_device}, build_cl_options.c_str()) != CL_SUCCESS) {
        std::cout << "Error building: " << 
        program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(default_device) << std::endl;
        exit(1);
    }

    // Copy the host memory buffer to their respective memory buffers
    // push write commands to queue
    queue.enqueueWriteBuffer(data_mem_obj, CL_TRUE, 0,
        p->sw_count * sizeof(DataState), data_buffer);

    queue.enqueueWriteBuffer(p_mem_obj, CL_TRUE, 0, 
        sizeof(NormalParameters), p);

    cl::Kernel stormer_verlet(program, "stormer_verlet");
    stormer_verlet.setArg(0, data_mem_obj);
    stormer_verlet.setArg(1, p_mem_obj);

    // Execute the OpenCL kernel on the list
    int global_item_size = p->sw_count;
    int local_item_size  = p->threads;

    queue.enqueueNDRangeKernel(stormer_verlet, cl::NullRange,
        cl::NDRange(global_item_size),cl::NDRange(local_item_size));

    queue.finish();

    //// read result from GPU to here
    queue.enqueueReadBuffer(data_mem_obj, CL_TRUE, 0,
        p->sw_count * sizeof(DataState), data_buffer);

}
