// Licensed after GNU GPL v3

#include "data_structures.h"
#include "xorwow_gen.h"
#include "user_types.h"

 // Max private memory buffer 
#define BUFF_SIZE 10
//#define PHASE_CALC

typedef struct {
    double time;
    double i_sw;
    bool   flag;
} Result;

__kernel void stormer_verlet(__global DataState * data_buffer,
        __constant NormalParameters * p)
{
    unsigned g_id = get_global_id(0);

    double c_s_h = p->t_step * 0.5;
    double c_b   = 1.0 / (1.0 + p->alpha * c_s_h);
    double c_a   = c_b / (1.0 + p->alpha * c_s_h);
    double c_s_b = c_b * p->t_step;

    double c_b_intensity = c_b * p->intensity;
    double c_s_vel   = p->t_step * p->i_vel;
    double c_s_omega = p->t_step * p->omega;

    double fi      = asin(p->i_bias);
    double v       = 0.0;
    double fi_next = 0.0;

    // precalc coef. var
    double sin_fi      = 0.0;
    double sin_fi_next = 0.0;
    double i_sw        = 0.0;

    double p_bound = M_PI_F + asin(p->i_osc - p->i_bias);
    Result phase = {.time = 0.0, .i_sw = 0.0, .flag = true};

    double v_bound = 0.5 / p->alpha;
    Result rate  = {.time = 0.0, .i_sw = 0.0, .flag = true};
    // Exponential smoothing, V(t) var.
    double v_smth  = 0;

    XorwowState xws;
    xws.x[0]    = data_buffer[g_id].seed;
    xws.counter = 0;

    float noise_buffer[BUFF_SIZE];
    double tmp_value = 0.0;
    size_t accum = 0;

    for(size_t i = 0; i < p->t_size; i += BUFF_SIZE) {

        if(!rate.flag || i_sw > 1.0)
            break;

        normal_noise_gen(&xws, noise_buffer, BUFF_SIZE);

        for(size_t j = 0; j < BUFF_SIZE; ++j) {

            i_sw = p->i_bias + c_s_vel * accum;

            tmp_value = c_b_intensity * noise_buffer[j] + c_s_b * i_sw;
#ifdef IOSC
            tmp_value += c_s_b * p->i_osc * sin(c_s_omega * accum);
#endif
            sin_fi = sin((fp_)fi);
#ifdef IASYM
            sin_fi = p->asym_coef * (sin_fi + p->snd_harm * sin((fp_)(2.0 * fi)));
#endif
            fi_next = fi + c_s_b * (v - c_s_h * sin_fi) + c_s_h * tmp_value;

            sin_fi_next = sin((fp_)fi_next);
#ifdef IASYM
            sin_fi = p->asym_coef * (sin_fi_next + 
                     p->snd_harm * sin((fp_)(2.0 * fi_next)));
#endif
            v = c_a * v - c_s_h * (c_a * sin_fi + sin_fi_next) + tmp_value;

            fi = fi_next;

            // Voltage exponential smoothing
            v_smth += p->r_window * (v - v_smth);

            if(v_smth < v_bound && rate.flag) {
                rate.time = accum;
                rate.i_sw = i_sw;
            } else {
                rate.flag = false;
            }
#ifdef PHASE_CALC
            p_bound = M_PI_F + asin(p->i_osc - i_sw);
            if(fi < p_bound && fi > -p_bound && phase.flag) {
                phase.time = accum;
                phase.i_sw = i_sw;
            } else {
                phase.flag = false;
            }
#endif
            accum++;
        }
    }
    data_buffer[g_id].phase_mv   = phase.time;
    data_buffer[g_id].phase_sd   = phase.time * (phase.time + 1.0) / 2.0;
    data_buffer[g_id].phase_i_sw = phase.i_sw;

    data_buffer[g_id].rate_mv    = rate.time;
    data_buffer[g_id].rate_sd    = rate.time * (rate.time + 1.0) / 2.0;
    data_buffer[g_id].rate_i_sw  = rate.i_sw;
}
