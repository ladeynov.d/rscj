#!/bin/bash

ls | grep Makefile > .make_list

echo "Build list:"
cat .make_list
echo ""

for n in $(cat .make_list)
do
    echo ">> build " $n
    make -f $n clean
    make -f $n
done

rm .make_list
